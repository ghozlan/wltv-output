# Multi-Layer Transmission in Wideband Linear Time-Varying Channels

multilayer_script: https://htmlpreview.github.io/?https://bitbucket.org/ghozlan/wltv-output/raw/master/html/multilayer_script.html

sim_wltv_script: https://htmlpreview.github.io/?https://bitbucket.org/ghozlan/wltv-output/raw/master/html/sim_wltv_script.html

# MATLAB

### See the code and its output in a friendly format (html)!
* Click [multilayer_script](https://htmlpreview.github.io/?https://bitbucket.org/ghozlan/wltv-output/raw/master/html/multilayer_script.html) to see **signal spectra** (input to and output from the channel) and **information rates** achieved by various receivers for
      * Channel E, Scheme 1   
      * Note: **scroll down after you click above** to see the figures!
<!---
* Click [sim_wltv_script](blank) to see **spectra** and **rates** for
      * Channel A, Scheme 1   
      * Channel A, Scheme 2   
      * Channel E, Scheme 1  
      * Note: less code is revealed here than in multilayer_script. Also, do not forget to scroll down after you click to see the figures!
* Click [load_batch_script](blank) to see **comparisons** between the information rates of Scheme 1 and Scheme 2.